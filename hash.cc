//*****************************************************************/
//
// Copyright (C) 2006-2009 Seung-Jin Sul
//      Department of Computer Science
//      Texas A&M University
//      Contact: sulsj@cs.tamu.edu
//
//      CLASS IMPLEMENTATION
//      HashRFMap: Class for hashing bit
//
//*****************************************************************/

//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.

#include "hash.hh"
#include "hashfunc.hh"


void
HashRFMap::uhashfunc_init(
    size_t t,
    size_t n,
    size_t c)
{
    _HF.UHashfunc_init(t, n, c);
}

void
HashRFMap::uhashfunc_init(
    size_t t,
    size_t n,
    size_t c,
    size_t newSeed)
{
    _HF.UHashfunc_init(t, n, c, newSeed);
}

void
HashRFMap::hashing_bitstr(
    size_t      treeIdx,
    size_t      numTaxa,
    uint64_t    hv1,
    uint64_t    hv2,
    float       dist,
    bool        wOption)
{
    ///////////////////////////////
    // double linked list
    ///////////////////////////////
    size_t sizeVec = _hashtab2[hv1].size();
    if (sizeVec > 0) {
        bool found = false;
        for (size_t i = 0; i < sizeVec; ++i) {
            if (_hashtab2[hv1][i]._hv2 == hv2) {
                _hashtab2[hv1][i]._vecTreeIdx.push_back(treeIdx);
                if (wOption)
                    _hashtab2[hv1][i]._vecDist.push_back(dist);
                found = true;
                break;
            }
        }
        if (!found) {
            TREEIDX_STRUCT_T bk2;
            bk2._hv2 = hv2;
            bk2._vecTreeIdx.push_back(treeIdx);
            if (wOption)
                bk2._vecDist.push_back(dist);
            _hashtab2[hv1].push_back(bk2);
        }
    }
    else if (sizeVec == 0) {
        TREEIDX_STRUCT_T bk2;
        bk2._hv2 = hv2;
        bk2._vecTreeIdx.push_back(treeIdx);
        if (wOption)
            bk2._vecDist.push_back(dist);
        _hashtab2[hv1].push_back(bk2);
    }
}

void
HashRFMap::hashrfmap_clear()
{
    _hashtab.clear();
    _hashtab2.clear();
}

// eof

